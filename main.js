window.onload = OnLoad;

// NOTICE: Cookies dont work on local files on chromium. Firefox seems to work



function Debugs(){
    // alert("load")
    let output = document.querySelector("#output")

    let setButton = document.querySelector("button#get")
    setButton.onclick = function(){
        // alert("Get")
        let cookie = JSON.parse(Cookies.get('time'))
        output.innerText = cookie.time + " name: " + cookie.name
    }
    
    let getButton = document.querySelector("button#set")
    getButton.onclick = function(){
        // alert("Set")
        let cookie = {time:new Date().getSeconds(), name:"kekkonen"}
        Cookies.set('time', JSON.stringify(cookie))
    }

    let clearButton = document.querySelector("button#clear")
    clearButton.onclick = function(){
        // alert("Clear")
        Cookies.remove('time')
    }
}

let toggleButton;
let totalText;
let timerOn = false;
let intervalId;
let startDate;
let totalWasted;
function TimerInterval(){
    UpdateTimerText()
}

function RunningTimeSpan(){
    var date1 = startDate;
    var date2 = new Date();
    let timespan = DatesToTimeSpan(date1, date2)
    return timespan;
}

function UpdateTimerText(){
    let wasted = totalWasted
    if(timerOn){
        let timespan = RunningTimeSpan()
        wasted = AddTimeSpan(timespan, totalWasted)
        toggleButton.innerHTML = GetTimeSpanText(timespan)
    }
    totalText.innerText = GetTimeSpanText(wasted)
}

function MsToTimeSpan(ms){
    var days = Math.floor(ms / (1000 * 60 * 60 * 24));
    ms -=  days * (1000 * 60 * 60 * 24);

    var hours = Math.floor(ms / (1000 * 60 * 60));
    ms -= hours * (1000 * 60 * 60);

    var mins = Math.floor(ms / (1000 * 60));
    ms -= mins * (1000 * 60);

    var seconds = Math.floor(ms / (1000));
    ms -= seconds * (1000);

    let timespan = {
        days: days,
        hours: hours,
        minutes: mins,
        seconds: seconds
    }
    return timespan;
}
function TimeSpanToMs(timespan){
    let ms = timespan.seconds * 1000;
    ms += timespan.minutes * 60 * 1000;
    ms += timespan.hours * 60 * 60 * 1000;
    ms += timespan.days * 24 * 60 * 60 * 1000;
    return ms;
}
function DatesToTimeSpan(date1, date2){
    var ms = date2.getTime() - date1.getTime();
    return MsToTimeSpan(ms)    
}

function AddTimeSpan(span1, span2){
    let ms = TimeSpanToMs(span1) + TimeSpanToMs(span2)
    return MsToTimeSpan(ms)
}

function GetTimeSpanText(timespan){
    // let text = ""
    // let separator = "\n"
    // let titles=["days","hours","minutes","seconds"];
    // if(timespan.days > 0){
    //     text += timespan.days + " days" + separator
    // }
    // if(timespan.hours > 0){
    //     text += timespan.hours + " hours" + separator
    // }
    // if (timespan.minutes > 0){
    //     text += timespan.minutes + " minutes" + separator
    // }
    // text += timespan.seconds + " seconds"
    // return text;


    let nonZero = false
    let text = "";
    let entries = Object.entries(timespan);
    for(let i=0; i < entries.length; i++){
        const [key, value] = entries[i];
        nonZero = nonZero || value > 0
        if(nonZero || i == entries.length - 1){
            text += value + " " + key + "\n"
        }
    }
    // for (const [key, value] of entries) {
    //     // console.log(`${key}: ${value}`);
    //     nonZero = nonZero || value > 0
    //     if(nonZero){
    //         text += value + " " + key + "\n"
    //     }
    // }
    text = text.trimEnd("\n")
    return text;

    // let removable = "{}\"\\"
    // let json = JSON.stringify(timespan);
    // for(let i= 0; i < removable.length; i++){
    //     let char = removable.charAt(i);
    //     json = json.replaceAll(char,"")
    // }
    // json = json.replaceAll(",","\n")
    // return json;

    // toggleButton.innerHTML = days + " days, " + hours + " hours, " + mins + " minutes, " + seconds + " seconds";
}


function ToggleTimer(){
    // alert("Toggle");
    timerOn = !timerOn;
    if(timerOn){
        // toggleButton.innerHTML = currentTime = 0;
        // currentTime = 0;
        startDate = new Date();
        UpdateTimerText();
        intervalId = setInterval(TimerInterval,1000);
    }
    else{
        clearInterval(intervalId)
        toggleButton.innerHTML = "Start";
        totalWasted = AddTimeSpan(totalWasted, RunningTimeSpan())
        SaveTimeWasted(totalWasted)
    }
}

const wastedTimeCookieKey = "totalWasted";
function LoadTimeWasted(){
    let wasted = LoadObject(wastedTimeCookieKey);
    if(wasted == null){
        wasted = MsToTimeSpan(0)
    }
    return wasted;
}

function SaveTimeWasted(timeWasted){
    SaveObject(wastedTimeCookieKey, timeWasted)
}

function LoadObject(key){
    var cookieData = Cookies.get(key)
    if(cookieData != null){
        return JSON.parse(cookieData);
    }
}

function SaveObject(key, data){
    Cookies.set(key, JSON.stringify(data), { sameSite: 'Lax', expires:999999 })
}

function OnLoad(){
    // Debugs();
    toggleButton = document.querySelector("#timertoggle");
    toggleButton.onclick = ToggleTimer;
    totalText = document.querySelector("#totaltext");
    totalWasted = LoadTimeWasted()
    UpdateTimerText();

    let clearButton = document.querySelector("#clear")
    clearButton.onclick = function(){
        if(timerOn){
            ToggleTimer()
        }
        let answer = prompt("Type \"clear\" to confirm clearing history")
        if(answer == "clear"){
            totalWasted = MsToTimeSpan(0);
            SaveTimeWasted(totalWasted)
            UpdateTimerText();
        }
    }
}

